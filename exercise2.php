<?php
/**
 *  Assumption of PHP 7+
 *  Given the above example data structure again. Write a PHP function/method to sort the data structure based on a key OR keys regardless of what level it or they occurs with in
 *  the data structure ( i.e. sort by last_name **AND** sort by account_id ). **HINT**: Recursion is your friend.
 */


$data = [['guest_id' => 177,
        'guest_type' => 'crew',
        'first_name' => 'Marco',
        'middle_name' => NULL,
        'last_name' => 'Burns',
        'gender' => 'M',
        'guest_booking' => [['booking_number' => 20008683,
                'ship_code' => 'OST',
                'room_no' => 'A0073',
                'start_time' => 1438214400,
                'end_time' => 1483142400,
                'is_checked_in' => true,
               ],
           ],
        'guest_account' => [['account_id' => 20009503,
                'status_id' => 2,
                'account_limit' => 0,
                'allow_charges' => true,
               ],
           ],
       ],
    ['guest_id' => 10000113,
        'guest_type' => 'crew',
        'first_name' => 'Bob Jr ',
        'middle_name' => 'Charles',
        'last_name' => 'Hemingway',
        'gender' => 'M',
        'guest_booking' => [['booking_number' => 10000013,
                'room_no' => 'B0092',
                'is_checked_in' => true,
               ],
           ],
        'guest_account' => [['account_id' => 10000522,
                'account_limit' => 300,
                'allow_charges' => true,
               ],
           ],
       ],
    ['guest_id' => 10000121,
        'guest_type' => 'crew',
        'first_name' => 'Chuck ',
        'middle_name' => 'James',
        'last_name' => 'Harris',
        'gender' => 'M',
        'guest_booking' => [['booking_number' => 10000013,
                'room_no' => 'B0092',
                'is_checked_in' => true,
               ],
           ],
        'guest_account' => [['account_id' => 10000521,
                'account_limit' => 300,
                'allow_charges' => true,
               ],
           ],
       ],
    ['guest_id' => 10000114,
        'guest_type' => 'crew',
        'first_name' => 'Al ',
        'middle_name' => 'Bert',
        'last_name' => 'Santiago',
        'gender' => 'M',
        'guest_booking' => [['booking_number' => 10000014,
                'room_no' => 'A0018',
                'is_checked_in' => true,
               ],
           ],
        'guest_account' => [['account_id' => 10000013,
                'account_limit' => 300,
                'allow_charges' => false,
               ],
           ],
       ],
    ['guest_id' => 10000115,
        'guest_type' => 'crew',
        'first_name' => 'Red ',
        'middle_name' => 'Ruby',
        'last_name' => 'Flowers ',
        'gender' => 'F',
        'guest_booking' => [['booking_number' => 10000015,
                'room_no' => 'A0051',
                'is_checked_in' => true,
               ],
           ],
        'guest_account' => [['account_id' => 10000519,
                'account_limit' => 300,
                'allow_charges' => true,
               ],
           ],
       ],
    ['guest_id' => 10000116,
        'guest_type' => 'crew',
        'first_name' => 'Ismael ',
        'middle_name' => 'Jean-Vital',
        'last_name' => 'Jammes',
        'gender' => 'M',
        'guest_booking' => [['booking_number' => 10000016,
                'room_no' => 'A0023',
                'is_checked_in' => true,
               ],
           ],
        'guest_account' => [
                ['account_id' => 10000015,
                'account_limit' => 300,
                'allow_charges' => true,
               ],
           ],
           ],
   ];

$cmpRecursive = function ($a, $b) use ($argv, &$mName){
    $return = 0; 
    array_shift($argv);
    foreach($a as $k=>$v){
        foreach($argv as $key){
            if($k === $key){
                return $a[$key] <=> $b[$key]; 
            }elseif(is_array($a[$k])){
                $return = call_user_func($mName,$a[$k], $b[$k]); 
                if($return === 0){
                    continue;
                }
            }
        }
        
    }
    return $return;
};
$mName = $cmpRecursive;
uasort($data, $cmpRecursive);
echo "<pre>";
print_r($data);