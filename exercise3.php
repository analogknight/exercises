<?php
/**
 * Please write two or more classes that allow for the setting and retrieval of the following information:
    - Customer Name
    - Customer Addresses
    - Items in Cart
    - Where Order Ships
    - Cost of item in cart, including shipping and tax
    - Subtotal and total for all items
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class Item {
    var $id;
    var $name;
    var $price;

    public function __construct($id, $name, $price)	{
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
    }

    public function getId()	{
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getPrice() {
        return $this->price;
    }

} 

class Customer {
    protected $first_name;
    protected $last_name;
    protected $addresses = [];

    public function __construct($first_name, $last_name, $addresses)	{
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->addresses = array_merge($this->addresses, $addresses);
    }

    public function getId()	{
        return $this->id;
    }

    public function getFirstName() {
        return $this->first_name;
    }

    public function getLastName() {
        return $this->last_name;
    }

    public function getAddresses($key=''){
        return (!empty($key)) ? $this->addresses[$key]:$this->getAddresses;
    }

}

class ShoppingCart implements Iterator, Countable {
    protected static $_instance = null;
    protected $items = [];
    protected $position = 0;
    protected $ids = [];
    protected $customer = [];
    protected $cartSubTotal = 0;
    protected $cartTotal = 0;
    
    public function __construct(&$customer) {
       $this->customer = $customer;
    }
    
    public function isEmpty() {
        return (empty($this->items));
    }
    
    public function addItem(Item $item) {
        $id = $item->getId();
        $this->ids[] = $id;
        if (!$id) throw new Exception('Item does not exist.');
        if (isset($this->items[$id])) {
            $this->updateItem($item, $this->items[$id]['qty'] + 1);
        } else {
            $this->items[$id] = array('item' => $item, 'qty' => 1);
            $this->cartTally();
        }
    }
    
    public function updateItem(Item $item, $qty) {
        $id = $item->getId();
        if ($qty === 0) {
            $this->deleteItem($item);
        } elseif ( ($qty > 0) && ($qty != $this->items[$id]['qty'])) {
            $this->items[$id]['qty'] = $qty;
            $this->cartTally();
        }
    } 
    
    public function deleteItem(Item $item) {
        $id = $item->getId();
        if (isset($this->items[$id])) {
            unset($this->items[$id]);
        }
        $index = array_search($id, $this->ids);
        unset($this->ids[$index]);
        $this->ids = array_values($this->ids);
        $this->cartTally();
    }
    
    public function getCart(){
        return $this->items;
    }
    
    public function getItemTotal(Item $item){
        $id = $item->getId();
        if (!isset($this->items[$id])) throw new Exception('Item is not in this cart.');
        $shippingAddress = $this->customer->getAddresses('shipping');
        $shipping = $this->getShippingChargeByLocale($shippingAddress['zip']);
        $tax = $this->getTaxByLocale($shippingAddress['zip']);
        return $this->calculateTotal($item, $tax, $shipping);
    }
    
    public function cartTally(){
        $this->cartSubTotal = 0;
        $shippingAddress = $this->customer->getAddresses('shipping');
        $shipping = $this->getShippingChargeByLocale($shippingAddress['zip']);
        $tax = $this->getTaxByLocale($shippingAddress['zip']);
        
        foreach($this->items as $item){
            $this->cartSubTotal += round($item['item']->price*$item['qty'],2);
        }
        $this->cartTotal = round($this->cartSubTotal *$tax, 2) + $this->cartSubTotal + $shipping  ;
        
    }
    
    public function getCartSubTotal(){
        return $this->cartSubTotal;
    }
    
    public function getCartTotal(){
        return $this->cartTotal;
    }
    
    public function getShippingChargeByLocale($locale){
        // assuming a look up call to a service
        return 19.99;
    }
    
    public function getTaxByLocale($locale){
        // assuming a look up call to a service
        return .07;
    }
    
    private function calculateTotal($item, $tax, $shipping = 0){
        $id = $item->getId();
        $price = $item->getPrice();
        $taxed = round(($price*$this->items[$id]['qty'])*$tax,2);
        return ($price*$this->items[$id]['qty'])+ $taxed +$shipping;
    }
    
    public function shippingAddress(){
        return $this->customer->getAddresses('shipping');
    }
    
    /* utilities */
    public function count() {
        return count($this->items);
    }
    
    public function key() {
        return $this->position;
    }
    public function next() {
        $this->position++;
    }
    public function rewind() {
        $this->position = 0;
    }
    public function valid() {
        return (isset($this->ids[$this->position]));
    }
    
    public function current() {
        $index = $this->ids[$this->position];
        return $this->items[$index];
    } 
    
    public static function getInstance()
    {
        if (!isset(static::$_instance)) {
            static::$_instance = new static;
        }
        return static::$_instance;
    }

    protected function __clone()
    {
    }
}

$item1 = new Item(1001, 'Widget A', 19.99);
$item2 = new Item(1002, 'Widget B', 21.99);
$item3 = new Item(239, 'Widget C', 12.99);


$customer = new Customer('Robert', 'Smith', ['mailing'=>['address_1'=>'551 Bay Ave','address_2'=>'','city'=>'Tampa','state'=>'FL','zip'=>'33625'],'shipping'=>['address_1'=>'223 Hampton Bay Circle','address_2'=>'','city'=>'Tampa','state'=>'FL','zip'=>'33616']]);

$cart = new ShoppingCart($customer);
$cart->addItem($item3);
$cart->addItem($item2);
$cart->addItem($item1);

$cart->addItem($item3);
$cart->addItem($item3);
$cart->addItem($item1);

echo "<pre>";
echo "\n\nCustomer Name: ".$customer->getFirstName()." ".$customer->getLastName();
echo "\n\nCustomer Mailing Adddress: \n\n".print_r($customer->getAddresses('mailing'));
echo "\n\n";
echo "\n\nCustomer Shipping Adddress: \n\n".print_r($customer->getAddresses('shipping'));
echo "Cart:";
echo "\n\n";
echo print_r($cart->getCart());
echo "\n\nItem ".$item2->getName().' Cost with shipping and tax: '.$cart->getItemTotal($item2); 
echo "\n\nCart SubTotal ".$cart->getCartSubTotal();
echo "\n\nCart Total ".$cart->getCartTotal();
echo "\n\n";
